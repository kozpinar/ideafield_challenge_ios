//
//  Model.swift
//  Challenge
//
//  Created by Onur Kozpınar on 19.12.2017.
//  Copyright © 2017 Kozpınar. All rights reserved.
//

import UIKit
import Charts


class Model {
    class var mainList: [(title: String, segue: String?)] {
        return [
            (
                title : "Rider Profile",
                segue : Segue.MainPresentRiderProfile
            ),
            (
                title: "adsasd",
                segue: nil
            )
        ]
    }
    
    
    class func pieData(count: Int, range: UInt32, colors: [UIColor] = [.cpOrange, .white]) -> PieChartData {
        var pieChartEntries: [PieChartDataEntry] = []
        for _ in 0..<count {
            let dRange = Double(range)
            pieChartEntries.append(PieChartDataEntry(value: Double(arc4random_uniform(range)) + dRange / 5))
        }
        
        let dataSet = PieChartDataSet(values: pieChartEntries, label: "")
        dataSet.drawIconsEnabled = false
        dataSet.drawValuesEnabled = false
        dataSet.colors = colors
        dataSet.valueLineWidth = 0
        dataSet.valueLineVariableLength = false
        
        return PieChartData(dataSet: dataSet)
    }
    
    class func lineChart(count: Int, range: UInt32, circleColor: UIColor, fillColor: UIColor) -> LineChartData {
        var entries: [ChartDataEntry] = []
        
        for i in 0..<count {
            let dRange = Double(range)
            let value = Double(arc4random_uniform(range)) + dRange * 0.1
            entries.append(ChartDataEntry(x: Double(i), y: value))
        }
        
        let dataSet = LineChartDataSet(values: entries, label: "")
        
        dataSet.drawIconsEnabled = false
        dataSet.drawFilledEnabled = true
        dataSet.cubicIntensity = 0.2
        dataSet.setCircleColor(circleColor)
        dataSet.circleRadius = 4
        dataSet.fillColor = fillColor
        dataSet.fillAlpha = 1
        dataSet.formLineWidth = 0
        dataSet.drawValuesEnabled = false
        dataSet.setColor(fillColor)
        
        return LineChartData(dataSet: dataSet)
    
    }
}



