//
//  UITableViewCell.swift
//  Challenge
//
//  Created by Onur Kozpınar on 19.12.2017.
//  Copyright © 2017 Kozpınar. All rights reserved.
//

import UIKit

extension UITableViewCell {
    class var reuseIdentifier: String {
        return String(describing: UITableViewCell.self)
    }
}
