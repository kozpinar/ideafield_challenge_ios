//
//  ViewController.swift
//  Challenge
//
//  Created by Onur Kozpınar on 19.12.2017.
//  Copyright © 2017 Kozpınar. All rights reserved.
//

import UIKit
import Charts

class RiderProfileViewController: UIViewController {
    @IBOutlet weak var goalPieChart: PieChartView!
    @IBOutlet weak var scorePieChart: PieChartView!
    @IBOutlet weak var shipmentLineChart: LineChartView!
    @IBOutlet weak var bottomChart: LineChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Common
    
    private func setup() {
        self.goalPieChart.data = Model.pieData(count: 2, range: 100)
        self.setup(pieChart: self.goalPieChart)
        self.goalPieChart.centerAttributedText =
            NSAttributedString(string: "$300",
                               attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 10),
                                            NSAttributedStringKey.foregroundColor : UIColor.black])
        
        
        self.scorePieChart.data = Model.pieData(count: 2, range: 100, colors: [.cpOrange, .lightGray])
        self.setup(pieChart: self.scorePieChart)
        self.scorePieChart.holeColor = .lightGray
        self.scorePieChart.centerAttributedText = NSAttributedString(string: "76",
                                                                      attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16),
                                                                                   NSAttributedStringKey.foregroundColor : UIColor.white])
 
        self.shipmentLineChart.data = Model.lineChart(count: 5, range: 100, circleColor: .orange, fillColor: .cpOrange)
        self.shipmentLineChart.setViewPortOffsets(left: 0, top: 10, right: 20, bottom: 20)
        self.setup(lineChart: self.shipmentLineChart)
        
        
        self.bottomChart.data = Model.lineChart(count: 10, range: 100, circleColor: .orange, fillColor: .cpOrange)
        
        self.bottomChart.setViewPortOffsets(left: 0, top: 10, right: 0, bottom: 0)
        self.setup(lineChart: self.bottomChart)
        
    }
    
    private func setup(pieChart: PieChartView) {
        pieChart.legend.enabled = false
        pieChart.isUserInteractionEnabled = false
        pieChart.transparentCircleRadiusPercent = 0
        pieChart.chartDescription?.enabled = false
    }
    
    private func setup(lineChart: LineChartView) {
        lineChart.legend.enabled = false
        lineChart.drawGridBackgroundEnabled = false
        
        lineChart.chartDescription?.enabled = false
        lineChart.isUserInteractionEnabled = false
        lineChart.pinchZoomEnabled = false
        
        let xAxis = lineChart.xAxis
        xAxis.drawGridLinesEnabled = false
        xAxis.drawAxisLineEnabled = false
        xAxis.enabled = false
        
        let yAxisLeft = lineChart.leftAxis
        yAxisLeft.enabled = false
        
        let yAxisRight = lineChart.rightAxis
        yAxisRight.enabled = false
        
    }
    
    
    // MARK: - Events
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

